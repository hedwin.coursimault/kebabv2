package fr.ahouais.kebab;
import java.util.ArrayList;

public class Kebab {

    private ArrayList<String> listeIngredients;
    private ArrayList<String> listeSauces;

    public Kebab(){
        this.listeIngredients = new ArrayList<>();
        this.listeSauces = new ArrayList<>();
    }

    public void addIngredient(String ingredient){
        this.listeIngredients.add(ingredient);
    }

    public void addSauce(String sauce){
        this.listeSauces.add(sauce);
    }

    public ArrayList<String> getIndredients() {
        return listeIngredients;
    }


    public boolean is_vege(String ingredient) {
        switch (ingredient) {
            case "viande":
                return false;
            case "poisson":
                return false;
            case "crevette":
                return false;
            default:
                return true;
        }
    }

    public boolean is_pece(String ingredient) {
        switch(ingredient) {
            case "viande":
                return false;
            default:
                return true;
        }
        
    }
} 