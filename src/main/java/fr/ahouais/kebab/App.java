package fr.ahouais.kebab;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App 
{
    
    public static void main( String[] args )
    {
        ArrayList<String> list = new ArrayList<String>();
        list.add("Viande");
        list.add("Crevettes");
        list.add("Poisson");
        list.add("Salade");
        list.add("Tomate");
        list.add("Oignon");
        list.add("Fromage");
        list.add("Algerienne");
        list.add("Mayonnaise");
        list.add("Ketchup");
        list.add("Moutarde");
        list.add("Samouraï");
        list.add("BBQ");
        list.add("Béchamel");
        list.add("Blanche");
        list.add("Double sauce");            
            
        System.out.println( "Ingredients : " );
        for(int i = 0; i< list.size(); i++){
            System.out.println(i +" "+ list.get(i) );
        }
        Scanner sc = new Scanner(System.in);

        System.out.println("Entrez les ingrédients un par un");
        String nextLine = sc.nextLine();
        Kebab kebab = new Kebab();
        while(nextLine != null && !nextLine.equals("") && !nextLine.equals("OK") && !nextLine.equals("ok") ){
            String ing = list.get(Integer.parseInt(nextLine));
            kebab.addIngredient(ing);
            nextLine = sc.nextLine();
        }
        System.out.println("Saisie terminé");
        boolean isVege = true;
        boolean isPece = true;
        for(int i = 0; i<kebab.getIndredients().size();i++){
            if(!kebab.is_vege(kebab.getIndredients().get(i))){
                isVege = false;
            }
            if(!kebab.is_pece(kebab.getIndredients().get(i))){
                isPece = false;
            }
        }
        sc.close();
        if(isVege){
            System.out.println("Le kebab est végétarien");
        }else{
            System.out.println("Le kebab n'est pas végétarien");
        }
        if(isPece){
            System.out.println("Le kebab est pescetarien");
        }else{
            System.out.println("Le kebab n'est pas pescetarien");
        }

        
    }
}
